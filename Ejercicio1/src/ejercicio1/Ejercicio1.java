package ejercicio1;

import java.io.File;
import java.util.Scanner;

public class Ejercicio1 {

    public static void main(String[] args){
        
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Directorio: ");           
        String sCarpAct = sc.nextLine();
        
        File carpeta = new File(sCarpAct);
        String[] listado = carpeta.list(); 
               
        if(carpeta.isDirectory() == false){
            System.out.println("No es un directorio");
        }else{
            if(listado == null || listado.length == 0){
                System.out.println("No hay elementos dentro de la carpeta actual");
                return;
            }else{
                for(int i=0;i<listado.length; i++){
                    System.out.println(listado[i]);
                }
            }
        }                             
    }   
}